#!/bin/bash

NS="${1:-default}"

echo - "Kubernetes Objects in Namespace: $NS"
echo "----------------------->"
echo -e "Deployments:"
kubectl -n $NS get deploy -o wide

echo "----------------------->"
echo -e "\nPods:"
kubectl -n $NS get pods -o wide

echo "----------------------->"
echo -e "\nServices:"
kubectl -n $NS get svc -o wide

echo "----------------------->"
echo -e "\nRoleBindings:"
kubectl -n $NS get rolebindings

echo "----------------------->"
echo -e "\nPersistent Volumes & Claims:"
kubectl -n $NS get pvc,pv -o wide

echo "----------------------->"
